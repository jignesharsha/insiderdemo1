package com.InsiderDemo.verifications;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.InsiderDemo.Init.Common;
import com.InsiderDemo.Init.SeleniumInit;


public class DemoIndex extends SeleniumInit {
	
	@Test	
	public void Demo()
	
	{
		Common common =  new Common(appiumDriver);/*int numberOfFailure=0;
		log("Test Case Id : TC_NewNote_01",11);
		log("<br></br> Step 1: Open the Notepad Application from mobile.");
		log("<br></br>Step 2: Add New Note Icon > New Note.");
		Common.pause(3);
		System.err.println(" tap on new note sub menu.............................................................");
		newNotePage=demoIndexPage.openAddNewNote();
		log("<br></br>Step 3: Verify New Note Page");
		Common.pause(3);
		if(newNotePage.verifyNewNotePage())
		{
			logStatus(1);
		}
		else
		{
			logStatus(2);
			numberOfFailure++;
		}
		log("<br></br>Step 4: Write note and save note ");
		newNotePage=demoIndexPage.addTitleAndNote();
		log("<br></br>Step 5: Verify added Note.");
		if(newNotePage.verifyaddedNote())
		{
			logStatus(1);
		}
		else
		{
			logStatus(2);
			numberOfFailure++;
		}
		if(numberOfFailure>0)
		{
			Assert.assertTrue(false);
		}*/
		int failureCount = 0;
		int logstep=1;
		Common.pause(2);
		Common.logcase("Insider_001 : To verify 'Skip' button functionality.");
		
		logstep=demoIndexPage.insiderlogoVerificationFreshApp(logstep);
		common.takeScreenshot();
		logstep=demoIndexPage.tapSkipBtn(logstep);
		
		logstep=demoIndexPage.selectCity(logstep);
		
		logstep=demoIndexPage.tapGetStarted(logstep);
		
		logstep=demoIndexPage.landingPageVerification(logstep);
		
		// Start Code inside you profile
		
		logstep=demoIndexPage.tapUserIcon(logstep);
		
		/*
		logstep=demoIndexPage.tapSelectCityAfterYou(logstep);
		
		logstep=demoIndexPage.changeCity(logstep);
		Common.pause(2);
		
		logstep=demoIndexPage.changeCityVerification(logstep);
		Common.pause(2);
		*/
		logstep=demoIndexPage.clickSignIn(logstep);
		Common.pause(2);
		
		logstep=demoIndexPage.clickGroup(logstep);
		Common.pause(2);
		
		logstep=demoIndexPage.ticketBuyFlow(logstep);
		Common.pause(2);
		
		logstep=demoIndexPage.verifyItemTotal(logstep);
	}
}

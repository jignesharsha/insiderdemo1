package com.InsiderDemo.Init;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

import java.io.File;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

import com.InsiderDemo.IndexPages.DemoIndexPage;
import com.InsiderDemo.Pages.AbstractPage;
import com.InsiderDemo.Pages.NewNotePage;

/**.+326
 * 
 * Selenium class Initialization
 * 
 * 
 */

//@Listeners(ATUReportsListener.class)
public class SeleniumInit implements ILoggerStatus ,ITestStatus{

	/* Minimum requirement for test configuration */
	protected String testUrl; // Test url
	protected String seleniumHub; // Selenium hub IP
	protected String seleniumHubPort; // Selenium hub port
	protected String targetBrowser; // Target browser
	protected static String test_data_folder_path = null;

	//Credentials for the Application when registered
	public String TransactionID="1";
	public String Password="1";
	
	// screen-shot folder
	protected static String screenshot_folder_path = null;
	public static String currentTest; // current running test

	protected static Logger logger = Logger.getLogger("testing");
	protected WebDriver webdriver;
	protected AndroidDriver<MobileElement>  appiumDriver;
	protected DesiredCapabilities capabilities;
	

	/* Page's declaration */
	
	protected AbstractPage abstractpage;
	
	Common common = new Common(appiumDriver);
	protected DemoIndexPage demoIndexPage;
	protected NewNotePage newNotePage;
	
	//protected AppiumDriver driver;

//	protected RegisteredPage registeredPage;
	// And many more ...

	/**
	 * Fetches suite-configuration from XML suite file.
	 * 
	 * @param testContext
	 */
	@BeforeTest(alwaysRun = true)
	public void fetchSuiteConfiguration(ITestContext testContext) {

	/*	testUrl = testContext.getCurrentXmlTest().getParameter("selenium.url");
		System.out.println("======"+testUrl+"=========");*/
		seleniumHub = testContext.getCurrentXmlTest().getParameter(
				"selenium.host");
		seleniumHubPort = testContext.getCurrentXmlTest().getParameter(
				"selenium.port");
		targetBrowser = testContext.getCurrentXmlTest().getParameter(
				"selenium.browser");
	}
	//
	
	/**
	 * WebDriver initialization
	 * 
	 * @return WebDriver object
	 * @throws MalformedURLException
	 * @throws InterruptedException
	 */
	/**
	 * WebDriver initialization
	 * 
	 * @return WebDriver object
	 * @throws Exception 
	 */
	@BeforeMethod(alwaysRun = true)
	public void setUp(Method method) throws Exception {		

		currentTest = method.getName(); // get Name of current test.

		URL remote_grid = new URL("http://" + seleniumHub + ":"
				+ seleniumHubPort + "/wd/hub");

		String SCREENSHOT_FOLDER_NAME = "screenshots";
		String TESTDATA_FOLDER_NAME = "test_data";

		test_data_folder_path = new File(TESTDATA_FOLDER_NAME)
				.getAbsolutePath();
		screenshot_folder_path = new File(SCREENSHOT_FOLDER_NAME)
				.getAbsolutePath();
		
		DesiredCapabilities capability = null;
		if (targetBrowser == null || targetBrowser.contains("Android"))
		{
			/*File file = new File("App/org.mightyfrog.android.simplenotepad-1.8.4-APK4Fun.com.apk");
			String apath = file.getAbsolutePath();
			//File app= new File("C:/Users/KQSPL/Desktop/MobiFinMobile/QA-Agent.apk");
			DesiredCapabilities capabilities = new DesiredCapabilities();
			//capabilities.setCapability("device","Android");
		capabilities.setCapability(CapabilityType.VERSION, "4.4.2");
			capabilities.setCapability(CapabilityType.PLATFORM, "Android");
			//capabilities.setCapability("platformName","Android");
		    capabilities.setCapability("deviceName","my_Android");
		   capabilities.setCapability("app",apath);
		  capabilities.setCapability("newCommandTimeout", 600);
		    capabilities.setCapability("fullReset","true");
		   // capabilities.setCapability("app-package","org.mightyfrog.android.simplenotepad");
			capabilities.setCapability("app-activity","org.mightyfrog.android.simplenotepad.SimpleNotepad");*/
			/*File AndroidApp = new File("D:\\Apps\\backups\\apps\\seatsFix-app-release.apk");
			String appPath = AndroidApp.getAbsolutePath();*/
			//String appPath = "/Ankit/project/Posski/Posski_copy.app";
			// Appium needs the path of app build
			// File app = new File(appPath);
			// Set up the desired capabilities and pass the iOS SDK version and
			// app path to Appium
			DesiredCapabilities capabilities = new DesiredCapabilities();
			//capabilities.setCapability(CapabilityType.BROWSER_NAME, "iOS");
			/*capabilities.setCapability(CapabilityType.VERSION, "7.0");
			capabilities.setCapability(CapabilityType.PLATFORM, "MAC");
			*/
			//capabilities.setCapability("app", appPath);

			//capabilities.setCapability("appium-version", "1.0");
			capabilities.setCapability("platformName", "Android");
			capabilities.setCapability("platformVersion", "23");
			capabilities.setCapability("autoAcceptAlerts",true);;
			
			//capabilities.setCapability("appPackage", "com.nis.app");
			capabilities.setCapability("appActivity", "in.insider.activity.SplashScreenActivity");
			/*capabilities.setCapability("fullReset", fullReset);*/
			capabilities.setCapability("noReset", "false");
			capabilities.setCapability("deviceName", "Android");
			
			//capabilities.setCapability("browserName", "safari");
			//capabilities.setCapability(CapabilityType.SUPPORTS_ALERTS,false);

					//
			 appiumDriver = new AndroidDriver(remote_grid, capabilities);
		   
		}
		else if(targetBrowser.contains("iphone"))
		{
			String appPath="";
			new DesiredCapabilities();
			//Appium needs the path of app build
			//File app = new File(appPath);
	        //Set up the desired capabilities and pass the iOS SDK version and app path to Appium 
	        DesiredCapabilities capabilities = new DesiredCapabilities();
	        capabilities.setCapability(CapabilityType.BROWSER_NAME, "iOS");
	        capabilities.setCapability(CapabilityType.VERSION, "7.0");
	        capabilities.setCapability(CapabilityType.PLATFORM, "MAC");
	        capabilities.setCapability("app", appPath);
	        //capabilities.iphone();
	        
	        //capabilities.setCapability("app","safari");
	        //capabilities = DesiredCapabilities.ipad();
	        
	        //Create an instance of RemoteWebDriver and connect to the Appium server.
	        //Appium will launch the BmiCalc App in iPhone Simulator using the configurations specified in Desired Capabilities
	        //driver = new SwipeableWebDriver(remote_grid, capabilities);
		}
		
		appiumDriver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
		demoIndexPage = new DemoIndexPage(appiumDriver);
		newNotePage = new NewNotePage(appiumDriver);
	
		}
	

	/**
	 * After Method
	 * 
	 * @param testResult
	 */
	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult testResult) {
		
		try {

			String testName = testResult.getName();

			if (!testResult.isSuccess()) {

				/* Print test result to Jenkins Console */
				System.out.println();
				System.out.println("TEST FAILED - " + testName);
				System.out.println();
				System.out.println("ERROR MESSAGE: "
						+ testResult.getThrowable());
				System.out.println("\n");
				Reporter.setCurrentTestResult(testResult);

				/* Make a screenshot for test that failed */
				String screenshotName = common.getCurrentTimeStampString()
						+ testName;
				
				System.out.println("========++++++"+common.getCurrentTimeStampString()+"========++++++++++");
				Reporter.log("<br> <b>Please look to the screenshot - </b>");
				common.makeScreenshot(appiumDriver, screenshotName);
			} else {
				System.out.println("TEST PASSED - " + testName + "\n"); // Print
																		// test
																		// result
																		// to
																		// Jenkins
																		// Console
			}

			//driver.manage().deleteAllCookies();
			
			appiumDriver.quit();
			//driver1.closeApp();
			appiumDriver.resetApp();

		} catch (Throwable throwable) {
		}
	}

	/**
	 * Log given message to Reporter output.
	 * 
	 * @param msg
	 *            Message/Log to be reported.
	 */
	public static void logMessage(String msg) {
		Reporter.log("<br>" + msg + "</br>");
	}

	public static void log(String msg, final int logger_status) {

		switch (logger_status) {

		case ILoggerStatus.NORMAL:
			Reporter.log("<br>" + msg + "</br>");
			break;

		case ILoggerStatus.ITALIC:
			log("<i>" + msg + "</i>");
			break;

		case ILoggerStatus.STRONG:
			Reporter.log("<strong><img src=\"bullet_point.png\"/> " + msg + "</br>");
			break;
		}
	}

	public static void logStatus(final int test_status) {

		switch (test_status) {

		case ITestStatus.PASSED:
			Reporter.log("<font color=238E23>--<img src=\"passed.png\"/></font>");
			break;

		case ITestStatus.FAILED:
			Reporter.log("<font color=#FF0000>--<img src=\"failed.png\"/></font>");
			break;

		case ITestStatus.SKIPPED:
			Reporter.log("<font color=#FFFF00>--Skipped</font>");
			break;

		default:
			break;
		}

	}

	/**
	 * Log given message to Reporter output.
	 * 
	 * @param msg
	 *            Message/Log to be reported.
	 */
	public static void log(String msg) 
	{
		Reporter.log(msg);
	}

}

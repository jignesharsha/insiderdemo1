package com.InsiderDemo.IndexPages;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.InsiderDemo.Init.Common;
import com.InsiderDemo.Pages.AbstractPage;
import com.InsiderDemo.Pages.NewNotePage;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;

public class DemoIndexPage extends AbstractPage {
	public DemoIndexPage(AndroidDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	Common common =  new Common(appiumDriver);
	
	public NewNotePage openAddNewNote()
	{
		System.out.println("*******Add new Item Icon******");
		appiumDriver.findElementById("org.mightyfrog.android.simplenotepad:id/add_new").click();
		System.out.println("****** tap New Note******");
		appiumDriver.findElementByXPath("//*[contains(@text,'New note')]").click();
		Common.pause(5);
				
		return new NewNotePage(appiumDriver); 
	}
	public NewNotePage addTitleAndNote()
	{
		System.out.println("======== title========");
		appiumDriver.findElementById("org.mightyfrog.android.simplenotepad:id/edit_title").sendKeys("KiwiQa");
		Common.pause(2);
		System.out.println("========note========");
		appiumDriver.findElementById("org.mightyfrog.android.simplenotepad:id/edit_body").sendKeys("Hello, KiwiQA Pvt, Ltd ");
		Common.pause(2);
		System.out.println("========Save Image Icon========");
		appiumDriver.findElementByClassName("android.widget.ImageButton").click();
		Common.pause(2);
		System.out.println("========Save ========");
		appiumDriver.findElementById("android:id/title").click();
		Common.pause(2);
		System.out.println("========Back after save  ========");
		appiumDriver.findElementByClassName("android.widget.LinearLayout").click();
		Common.pause(2);
		return new NewNotePage(appiumDriver); 
	}
//////////////insider////
	
	public int insiderlogoVerificationFreshApp(int logstep)
	{
		Common.pause(2);
		Common.logstep("Step "+logstep+++" : Verify application opened successfully.");
		Common.pause(1);
		MobileElement insiderLogo = appiumDriver.findElement(By.id("in.insider.consumer:id/logo"));
		Common.pause(1);
		if(Common.isElementDisplayed(insiderLogo)){
			common.logStatus(1);
		}else{
			common.logStatus(2);
			Assert.assertTrue(false, "Application not opened.");
		}
		return logstep;
	}
	public int tapSkipBtn(int logstep)
	{
		try{
		Common.logstep("Step "+logstep+++" : Tap on 'Skip' button.");
		Common.pause(2);
		MobileElement skipBtn = appiumDriver.findElement(By.id("in.insider.consumer:id/btn_skip"));
		skipBtn.click();
		}catch(Exception e){
			Assert.assertTrue(false, "'Search' page is not appearing while clicking on 'Search' button.");
			common.takeScreenshot();
		}
		return logstep;
	}
	
	String currentCity;
	public int tapSelectCityAfterYou(int logstep)
	{
		try{
		Common.logstep("Step "+logstep+++" : Tap on 'Select City' icon.");
		Common.pause(1);
		MobileElement selectCity = appiumDriver.findElement(By.id("in.insider.consumer:id/tv_city"));
		Thread.sleep(2000);
		String[] temp = selectCity.getText().split(":");
		Thread.sleep(2000);
		currentCity = temp[1].trim();
		Thread.sleep(2000);
		Common.logstep("Step "+logstep+++" : Current selected city:- "+currentCity);
		selectCity.click();
		}catch(Exception e){
			Assert.assertTrue(false, "Not able to select city.");
			common.takeScreenshot();
		}
		return logstep;
	}
	
		
	public int tapSearchIcon(int logstep)
	{
		
		Common.logstep("Step "+logstep+++" : Tap on 'Search' icon.");
		Common.pause(1);
		MobileElement searchIcon = appiumDriver.findElement(By.id("in.insider.consumer:id/ll_search"));
		searchIcon.click();
		return logstep;
	}
	
	public int selectCity(int logstep)
	{
		try{
		Common.logstep("Step "+logstep+++" : Select 'City'.");
		Common.pause(1);
		List<MobileElement> cityRadioButtons = appiumDriver.findElements(By.id("in.insider.consumer:id/rb"));
		cityRadioButtons.get(2).click();
		}catch(Exception e){
			Assert.assertTrue(false, "City is not able to select.");
			common.takeScreenshot();
		}
		return logstep;
	}
	
	public int enterValueInSearchField(int logstep,String searchValue)
	{
		
		Common.logstep("Step "+logstep+++" : Enter value in 'Search' field.");
		Common.pause(1);
		MobileElement searchField = appiumDriver.findElement(By.id("in.insider.consumer:id/etxt_search"));
		searchField.sendKeys(searchValue);
		return logstep;
	}
	
	public int changeCity(int logstep) throws InterruptedException {
		try{
		Common.logstep("Step "+logstep+++" : Change the City.");
		Common.pause(1);
		
		List<MobileElement> cities = appiumDriver.findElements(By.id("in.insider.consumer:id/tv_city_name"));
		
		if(currentCity.equalsIgnoreCase("AMRITSAR")){
			Common.log("Selected city:- "+cities.get(3).getText());
			cities.get(2).click();
		}else{
			Common.log("Selected city:- "+cities.get(2).getText());
			cities.get(2).click();
		}
		}catch(Exception e){
			Assert.assertTrue(false, "Selected City not changed.");
			common.takeScreenshot();
		}
		return logstep;
	}
	
	public int doSearch(int logstep)
	{
		Common.logstep("Step "+logstep+++" : Tap on 'Search' icon from Keyboard.");
		//appiumDriver.hideKeyboard();
		appiumDriver.getKeyboard();
		//appiumDriver.pressKeyCode(84, AndroidKeyMetastate.META_NUM_LOCK_ON );
		appiumDriver.pressKeyCode(84);
		Common.log("keyboard");
		Common.pause(5);
		
		/*MobileElement searchField = appiumDriver.findElement(By.id("in.insider.consumer:id/etxt_search"));
		searchField.sendKeys(searchValue);*/
		return logstep;
	}
	
	public int tapGetStarted(int logstep)
	{
		try{
		Common.logstep("Step "+logstep+++" : Tap on 'Get Started' Button.");
		Common.pause(2);
		MobileElement getStarted = appiumDriver.findElement(By.id("in.insider.consumer:id/btn_finish"));
		getStarted.click();
		}catch(Exception e){
			Assert.assertTrue(false, "Not able to click on 'Get Started' button.");
			common.takeScreenshot();
		}
		return logstep;
	}
	
	
	public void swipeUp()
	{
		int height = appiumDriver.manage().window().getSize().getHeight();
		int width = appiumDriver.manage().window().getSize().getWidth();
		
		int height1 = height-(height-200);//height-300;
		int height2= height1/2-200;
		
		System.err.println("heght1-----------"+height1);
		int width1 =width/2;
		int endHeight=  height-100;
		System.err.println("This is height----"+height+"==========width==="+width);
		
		//appiumDriver.swipe(width1, endHeight, width1, height1, 2000);
		//appiumDriver.swipe(startx, starty, endx, endy, duration);
		Common.pause(3);
	
		//appiumDriver.swipe(513, 1670, 513, 480, 2000);
		Common.pause(3);
	}
	
	public void swipeDown()
	{
		int height = appiumDriver.manage().window().getSize().getHeight();
		int width = appiumDriver.manage().window().getSize().getWidth();
		
		int height1 =height -200;
		int height2= height1/2-200;
		
		System.err.println("heght2-----------"+height2);
		int width1 =width/2;
		int endHeight=  height-(height-200);
		System.err.println("This is height----"+height+"==========width==="+width);
		
		//appiumDriver.swipe(width1, endHeight, width1, height1, 2000);
		//appiumDriver.swipe(startx, starty, endx, endy, duration);
		Common.pause(3);
	
		//appiumDriver.swipe(513, 1670, 513, 480, 2000);
		Common.pause(3);
	}

	public String CurrentTitleName() {
		
		Common.pause(5);
		String newsTitle = appiumDriver.findElement(By.id("com.nis.app:id/news_title")).getText();
		
		return newsTitle;
	}

	public void tapOnScreen() {
		Common.pause(2);

		MobileElement storyText = appiumDriver.findElement(By.id("com.nis.app:id/news_text"));
		TouchAction tc = new TouchAction(appiumDriver);
		tc.tap(storyText).perform();
	} 

	public int searchPageVerification(int logstep) {
		Common.logvf("Step "+logstep+++" : Verify 'Search' page displayed while tapping on 'Search' icon.");
		Common.pause(1);
		MobileElement searchIcon = appiumDriver.findElement(By.id("in.insider.consumer:id/img_search"));
		Common.pause(1);
		if(Common.isElementDisplayed(searchIcon)){
			common.logStatus(1);
		}else{
			common.logStatus(2);
			Assert.assertTrue(false, "'Search' page is not appearing while clicking on 'Search' button.");
		}
		return logstep;
	}

	String cityAfterSelection;
	public int changeCityVerification(int logstep) {
		
		
		Common.logstep("Step "+logstep+++" : Verify selected city is visible on 'User' page.");
		MobileElement selectCity = appiumDriver.findElement(By.id("in.insider.consumer:id/tv_city"));
		String[] temp = selectCity.getText().split(":");
		cityAfterSelection = temp[1].trim();
		if(currentCity.equalsIgnoreCase(cityAfterSelection)){
			common.logStatus(1);
		}else{
			common.logStatus(2);
			Assert.assertTrue(false, "City is not visible.");
			common.takeScreenshot();
		}
		
		return logstep;
	}
	
	public int clickSignIn(int logstep) {
		try{
			Common.logstep("Step "+logstep+++" : Click 'SignIn' button. ");
			Common.pause(1);
			MobileElement signInBtn = appiumDriver.findElement(By.id("in.insider.consumer:id/tv_signinout"));
			Common.pause(1);
			signInBtn.click();
			
			MobileElement loginEmail = appiumDriver.findElement(By.id("in.insider.consumer:id/et_login_email"));
			loginEmail.sendKeys("bhavin.nayee@kiwiqa.com");
			
			MobileElement nextBtn = appiumDriver.findElement(By.id("in.insider.consumer:id/btn_login_next"));
			nextBtn.click();
			
			MobileElement password = appiumDriver.findElement(By.id("in.insider.consumer:id/et_password_password"));
			password.sendKeys("123456");
			
			MobileElement submitBtn = appiumDriver.findElement(By.id("in.insider.consumer:id/btn_submit_password"));
			submitBtn.click();
			
		}catch(Exception e){
			Assert.assertTrue(false, "'SignIn' button is not visible.");
		}
		
		return logstep;
	}
	
	public int clickGroup(int logstep) {
		try{
			Common.logstep("Step "+logstep+++" : Click 'Groups' link. ");
			Common.pause(1);
			MobileElement groupBtn = appiumDriver.findElement(By.id("in.insider.consumer:id/rl_groups"));
			Common.pause(1);
			groupBtn.click();
			
			Common.pause(2);
			TouchAction tapCoordinates = new TouchAction(appiumDriver); //where driver is AppiumDriver
			tapCoordinates.tap(51,254).perform();
			Common.pause(2);
			
		}catch(Exception e){
			Assert.assertTrue(false, "'Groups' link is not visible.");
		}
		
		return logstep;
	}
	
	public int ticketBuyFlow(int logstep) {
		try{
			Common.logstep("Step "+logstep+++" : Ticket buy flow. Continue.. ");
			Common.pause(1);
			MobileElement buyBtn = appiumDriver.findElement(By.id("in.insider.consumer:id/btn_buy"));
			Common.pause(1);
			buyBtn.click();
			
			Common.pause(1);
			MobileElement buyNowBtn = appiumDriver.findElement(By.id("in.insider.consumer:id/btn_eventdetail_buyalone"));
			Common.pause(1);
			buyNowBtn.click();
			
			Common.pause(1);
			MobileElement buyQtyBtn = appiumDriver.findElement(By.id("in.insider.consumer:id/btn_item_quantity"));
			Common.pause(1);
			buyQtyBtn.click();
			/*
			Common.pause(1);
			List<MobileElement> selectQtyList = appiumDriver.findElements(By.className("android.widget.Button"));
			Common.pause(1);
			Common.logstep("List= "+ selectQtyList.size());
			*/
			/*
			MobileElement selectQty = appiumDriver.findElement(By.className("android.widget.Button"));
			selectQty.sendKeys("1");
			*/
			appiumDriver.findElement(By.id("in.insider.consumer:id/btn_quantity_confirm")).click();
			
			Common.pause(1);
			MobileElement buyItemBtn = appiumDriver.findElement(By.id("in.insider.consumer:id/btn_item_buy"));
			Common.pause(1);
			buyItemBtn.click();
			Common.pause(1);
			/*
			MobileElement discountCode = appiumDriver.findElement(By.id("in.insider.consumer:id/et_cart_discount_form_code"));
			Common.pause(1);
			discountCode.sendKeys("KRUPEN");
			Common.pause(1);
			
			MobileElement applyBtn = appiumDriver.findElement(By.id("in.insider.consumer:id/btn_discount_form_apply"));
			Common.pause(1);
			applyBtn.click();
			
			appiumDriver.hideKeyboard();
			
			Common.pause(1);
			MobileElement confBtn1 = appiumDriver.findElement(By.id("in.insider.consumer:id/btn_cart_confirm"));
			Common.pause(1);
			confBtn1.click();
			
			Common.pause(1);
			MobileElement confBtn2 = appiumDriver.findElement(By.id("in.insider.consumer:id/btn_login_already_next"));
			Common.pause(1);
			confBtn2.click();
			
			Common.pause(1);
			
			TouchAction tapCoordinates = new TouchAction(appiumDriver); //where driver is AppiumDriver
			tapCoordinates.tap(42,682).perform();
			Common.pause(2);
			
			Common.pause(1);
			MobileElement confBtn3 = appiumDriver.findElement(By.id("in.insider.consumer:id/btn_confirm_details"));
			Common.pause(1);
			confBtn3.click();
			*/
			
		}catch(Exception e){
			Assert.assertTrue(false, "Exception: "+e);
		}
		
		return logstep;
	}
	
	public int showMyTickets(int logstep)
	{
		try{
		Common.logstep("Step "+logstep+++" : Show my tickets.");
		Common.pause(2);
		MobileElement showticket = appiumDriver.findElement(By.id("in.insider.consumer:id/btn_monkey_show_tickets"));
		showticket.click();
		}catch(Exception e){
			Assert.assertTrue(false, "'Show my tickets button is not visible.");
			common.takeScreenshot();
		}
		return logstep;
	}
	
	public int landingPageVerification(int logstep)
	{
		try{
			Common.logstep("Step "+logstep+++" : Verify 'Landing' page displayed while tapping on 'Get started' button.");
			Common.pause(2);
			MobileElement searchIcon = appiumDriver.findElement(By.id("in.insider.consumer:id/ll_search"));
			MobileElement goOutText = appiumDriver.findElement(By.id("in.insider.consumer:id/txt_goout_subtitle"));
			MobileElement stayInText = appiumDriver.findElement(By.id("in.insider.consumer:id/txt_stayin_subtitle"));
			Common.pause(2);
			
			if(Common.isElementDisplayed(goOutText) && Common.isElementDisplayed(stayInText)){
				if(Common.isElementDisplayed(searchIcon)){
					common.logStatus(1);
				}else{
					common.logStatus(2);
				}
				
			}
		}catch(Exception e){
			Assert.assertTrue(false, "Element is not available on Landing page.");
			common.takeScreenshot();
		}
		return logstep;
	}
	
	public int tapUserIcon(int logstep)
	{
		Common.logstep("Step "+logstep+++" : Tap on 'You' icon.");
		Common.pause(1);
		MobileElement youIcon = appiumDriver.findElement(By.id("in.insider.consumer:id/ll_profile"));
		youIcon.click();
		return logstep;
	}
	public int verifyItemTotal(int logstep)
	{
		try{
		Common.logstep("Step "+logstep+++" : Verify item total.");
		Common.pause(2);
		MobileElement subTotal = appiumDriver.findElement(By.id("in.insider.consumer:id/tv_cart_item_totalprice"));
		MobileElement convenienceFee = appiumDriver.findElement(By.id("in.insider.consumer:id/tv_cart_convenience_fee"));
		MobileElement total = appiumDriver.findElement(By.id("in.insider.consumer:id/tv_cart_total_cost"));
		String subTotaltxt = new String(subTotal.getText().getBytes(),"UTF-8");
		String convenienceFeetxt = new String(convenienceFee.getText().getBytes(),"UTF-8");
		String totaltxt = new String(total.getText().getBytes(),"UTF-8");
		//String subTotaltxt1 = new String(subTotaltxt.getBytes("UTF-8"));
		
		//byte[] byteText = subTotaltxt.getBytes("UTF-8");
		//To get original string from byte.
		//String subTotaltxt1= new String(byteText , "UTF-8");
		
		
		String[] subTotaltxt1= subTotaltxt.split("\\?");
		String[] convenienceFeetxt1= convenienceFeetxt.split("\\?");
		String[] totaltxt1= totaltxt.split("\\?");
		
		int subQty = Integer.parseInt(subTotaltxt1[1]);
		int convenienceFeeTotal = Integer.parseInt(convenienceFeetxt1[1]);
		int matchTotal = subQty+convenienceFeeTotal;
		System.out.println("--------matchTotal------ "+matchTotal);
		String matchTotaltxt = String.valueOf(matchTotal);
		
		if (matchTotaltxt.equalsIgnoreCase(totaltxt1[1])){
			Common.pause(1);
			common.logStatus(1);
			
		}else{
			Common.pause(2);
		}
		//showticket.click();
		}catch(Exception e){
			common.logStatus(2);
			Assert.assertTrue(false, "Total is not verified.");
			
			common.takeScreenshot();
		}
		return logstep;
	}
	
}

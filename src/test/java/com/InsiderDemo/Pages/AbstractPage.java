package com.InsiderDemo.Pages;

import com.InsiderDemo.Init.SeleniumInit;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;


public abstract class AbstractPage extends SeleniumInit {

	//Common common = new Common(driver);

	public int DRIVER_WAIT = 5;

	/**
	 * Initialize UserAbstractPage.
	 * 
	 * @param Driver
	 *            .
	 */
	public AbstractPage(AndroidDriver driver) {
		this.appiumDriver = driver;
	}
	
}
package com.InsiderDemo.Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.InsiderDemo.Init.Common;

import io.appium.java_client.android.AndroidDriver;

public class NewNotePage extends AbstractPage {

	public NewNotePage(AndroidDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	public boolean verifyNewNotePage()
	{
		if(Common.isElementDisplayed(appiumDriver.findElementById("org.mightyfrog.android.simplenotepad:id/edit_body")))
			return true;
		else
			return false;
	}
	public boolean verifyaddedNote()
	{
		if(Common.isElementDisplayed(appiumDriver.findElementByXPath("//*[contains(@text,'KiwiQa')]")))
			return true;
		else
			return false;
	}
	

}
